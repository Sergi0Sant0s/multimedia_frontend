import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from './pages/Home'
import About from './pages/About'
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import Subjects from "./pages/Subsjects";

function Routes() {
      return (
            <BrowserRouter>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/profile" component={Profile} />
                  <Route exact path="/about" component={About} />
                  <Route exact path="/courses" component={Courses} />
                  <Route exact path="/subjects" component={Subjects} />
            </BrowserRouter>
      );
}

export default Routes;