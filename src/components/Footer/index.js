import React from 'react'

const Footer = () => {
      return (
            <>
            <div
            style={{
                  display: "flex",
                  padding: "auto",
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
            minHeight:"40px"
            }}
            >
            <h3>{process.env.REACT_APP_FOOTER_NAME}</h3>
            </div>
            </>

      )
}

export default Footer
