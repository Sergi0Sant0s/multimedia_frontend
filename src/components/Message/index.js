import React from 'react'
import {
  Collapse,
  IconButton
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import Alert from '@material-ui/lab/Alert';

const Message = (props) => {
  return (
    <Collapse in={props.state.open} style={{ position: "absolute", bottom: "75px", right: "25px" }}>
      <Alert
        severity={props.messageIco}
        action={
          <IconButton
            aria-label="close"
            color="red"
            size="small"
            onClick={() => {
              props.state.close(false);
            }}
          >
            <CloseIcon fontSize="inherit" />
          </IconButton>
        }
      >
        {props.message}
      </Alert>
    </Collapse>
  )
}

export default Message
