import React from 'react'
import {
      Modal as ModalComp,
} from '@material-ui/core'


const Modal = ({ isShowing, hide, title, message }) => {
      if (isShowing) {
            const body = (
                  <div>
                        <h2 id="simple-modal-title">{title}</h2>
                        <p id="simple-modal-description">{message}</p>
                  </div>
            );

            return (
                  <ModalComp
                        open={isShowing}
                        onClose={hide}
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                  >
                        {body}
                  </ModalComp>
            )
      }
}

export default Modal
