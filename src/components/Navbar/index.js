import React, {useState, Fragment} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Drawer,
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Avatar,
  List,
  ListSubheader,
  ListItemText,
  ListItemIcon,
  ListItemAvatar,
  ListItem,
  Paper,
  Badge,
  Button
} from '@material-ui/core'
import {
    Menu as MenuIcon,
    ChevronLeft as ChevronLeftIcon,
    ChevronRight as ChevronRightIcon,
    MoveToInbox as InboxIcon,
    Mail as MailIcon,
    Notifications as NotificationsIcon,
    AccountCircle
} from '@material-ui/icons'
import messages from './notifications.json'
import {colors} from '../../utils/colors.js'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: colors.default
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
      menuProfile: {
            padding: "10px 20px"
      },
      button: {
            color: colors.white,
            borderColor: colors.white,
            marginLeft: "5px",
            backgroundColor: "transparent",
            '&:hover': {
                  background: colors.white,
                  color: colors.default
            }

      },
      menus:{
        top: "60px !important"
      },
  subheader: {
    position: "relative"
  }
}));

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [profile, setProfile] = useState(null);
  const [notifications, setNotifications] = useState(null);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  
  const isMenuOpenProfile = Boolean(profile);
  const isMenuOpenNotifications = Boolean(notifications);

  const handleProfileMenuOpen = (event) => {
        setProfile(event.currentTarget);
  };
  const handleNotificationsMenuOpen = (event) => {
        setNotifications(event.currentTarget);
  };
  const handleMenuClose = (menu) => {
        switch (menu) {
              case 'profile':
                    setProfile(null);
                    break;
              case 'notifications':
                    setNotifications(null);
                    break;
              default:
                    break;
        }
  };

  const goTo = (href) => {
    window.location.href = href
  }

  const logout = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("expire");
        window.location.href = "/";
  };
  const profileClick = () => {
        if(localStorage.getItem("token") != null)
            window.location.href = "/profile";
  };

  /*IDS */
  const menuProfileId = 'primary-search-profile-menu';
  const menuNotificationId = 'primary-search-notification-menu';

  /* Menus */
  const renderMenu = (
    <Menu
              profile={profile}

              id={menuProfileId}
              keepMounted
      style={{ top: "-175px", right: "0px", left: "1560px" }}
              open={isMenuOpenProfile}
              onClose={() => handleMenuClose("profile")}
        >
              <MenuItem className={classes.menuProfile} onClick={profileClick}>Profile</MenuItem>
              <MenuItem className={classes.menuProfile} onClick={logout}>Logout</MenuItem>
        </Menu>
  );

  const renderNotifications = (
    <Menu
              profile={notifications}

              id={menuNotificationId}
              keepMounted
      style={{ top: "-165px", right: "0px", left: "1000px", height: "500px" }}
              open={isMenuOpenNotifications}
              onClose={() => handleMenuClose("notifications")}
        >
      <Typography className={classes.text} variant="h5" gutterBottom style={{ margin: "15px" }}>
                    Notifications
                    </Typography>
      <Paper>
        <List className={classes.list} style={{ padding: "0 !important" }}>
                          {messages.map(({ id, primary, secondary, person, url }) => (
                                <Fragment key={id}>
                                      {id === 1 && <ListSubheader className={classes.subheader}>Today</ListSubheader>}
                                      {id === 3 && <ListSubheader className={classes.subheader}>Yesterday</ListSubheader>}
                                      <ListItem button onClick={() => {window.location.href="/login"}}>
                                            <ListItemAvatar>
                                                  <Avatar alt="Profile Picture" src={person} />
                                            </ListItemAvatar>
                                            <ListItemText primary={primary} secondary={secondary} />
                                      </ListItem>
                                </Fragment>
                          ))}
                    </List>
              </Paper>
        </Menu>
  );
  let auth = localStorage.getItem('token') != null ? true : false;


  const menus = [
    {text:"Home",href:"/"},
    {text:"Cursos",href:"/courses"},
    {text:"Disciplinas",href:"/subjects"},
    {text:"Notas",href:"/classifications"},
  ]
  return (
        <div style={{minHeight:"65px"}}>
      {renderMenu}
      {renderNotifications}
      <CssBaseline />
      <AppBar
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
            
        <Toolbar style={{width: "100%", justifyContent: "space-between"}}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            <Button href="/" style={{color:colors.white, fontSize:"30px"}}>{process.env.REACT_APP_APP_NAME}</Button>
          </Typography>
              <div className={classes.sectionDesktop}>
          {auth ? (
            <>
                                          <IconButton
                                                color="inherit"
                                                aria-controls={menuProfileId}
                                                aria-haspopup="true"
                                                onClick={handleNotificationsMenuOpen}>
                                                <Badge badgeContent={messages.length} color="secondary">
                                                      <NotificationsIcon/>
                                                </Badge>
                                          </IconButton>
                                          <IconButton
                                                edge="end"
                                                aria-label="account of current user"
                                                aria-controls={menuNotificationId}
                                                aria-haspopup="true"
                                                onClick={handleProfileMenuOpen}
                                                color="inherit"
                                          >
                                                <AccountCircle />
                                          </IconButton>
                                          </>
                              ) :
                                    (
                                      <>
                                                <Button variant="outlined" href="/register" className={classes.button}>
                                                      Registar
                                                </Button>
                                                <Button variant="outlined" href="/login" className={classes.button}>
                                                      Iniciar Sessão
                                                </Button>
                                                </>
                                    )}
                                          </div>
                                          
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {menus.map((item, index) => (
            <ListItem button key={item.text} onClick={() => goTo(item.href)}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItem>
          ))}
        </List>
        <Divider />
      </Drawer>
      </div>
  );
}
