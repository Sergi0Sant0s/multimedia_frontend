import React, { useState, Fragment } from 'react';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import {
      AppBar,
      Toolbar,
      IconButton,
      Typography,
      Badge,
      MenuItem,
      Menu,
      Avatar,
      ListSubheader,
      ListItemText,
      ListItemAvatar,
      ListItem,
      List,
      Paper,
      Button

} from '@material-ui/core';
import { Menu as MenuIcon, AccountCircle, Notifications as NotificationsIcon } from '@material-ui/icons';
import messages from './notifications.json'


const useStyles = makeStyles((theme) => ({
      menus: {
            marginTop: "20px"
      },
      menuProfile: {
            padding: "10px 20px"
      },
      grow: {
            flexGrow: 1,
      },
      title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                  display: 'block',
            },
      },
      subheader: {
            backgroundColor: theme.palette.background.paper,
      },
      paper: {
            paddingBottom: 50,
      },
      list: {
            marginBottom: theme.spacing(0),
            maxHeight: "250px",
            maxWidth: "350px",
      },
      text: {
            padding: theme.spacing(2, 2, 0),
      },
      sectionDesktop: {
            root: {
            '& > *': {
                  margin: theme.spacing(1),
            },
            },
      },
      button: {
            color: "white",
            borderColor: "white",
            marginLeft: "5px",
            backgroundColor: "transparent",
            '&:hover': {
                  background: "#fff",
                  color: "black"
            }

      }
}));

const NavBar = () => {
      const history = useHistory();
      const classes = useStyles();
      const [profile, setProfile] = useState(null);
      const [notifications, setNotifications] = useState(null);

      const isMenuOpenProfile = Boolean(profile);
      const isMenuOpenNotifications = Boolean(notifications);

      const goToPage = () => {
            history.push("/login")
            /*TODO Mark notification viewed*/
      }

      const handleProfileMenuOpen = (event) => {
            setProfile(event.currentTarget);
      };
      const handleNotificationsMenuOpen = (event) => {
            setNotifications(event.currentTarget);
      };
      const handleMenuClose = (menu) => {
            switch (menu) {
                  case 'profile':
                        setProfile(null);
                        break;
                  case 'notifications':
                        setNotifications(null);
                        break;
                  default:
                        break;
            }
      };

      const logout = () => {
            localStorage.removeItem("token");
            window.location.href = "/";
      }

      /*IDS */
      const menuProfileId = 'primary-search-profile-menu';
      const menuNotificationId = 'primary-search-notification-menu';

      /* Menus */
      const renderMenu = (
            <Menu
                  className={classes.menus}
                  profile={profile}
                  anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                  id={menuProfileId}
                  keepMounted
                  transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                  open={isMenuOpenProfile}
                  onClose={() => handleMenuClose("profile")}
            >
                  <MenuItem className={classes.menuProfile} onClick={handleMenuClose}>Profile</MenuItem>
                  <MenuItem className={classes.menuProfile} onClick={logout}>Logout</MenuItem>
            </Menu>
      );
      const renderNotifications = (
            <Menu
                  className={classes.menus}
                  profile={notifications}
                  anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                  id={menuNotificationId}
                  keepMounted
                  transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                  open={isMenuOpenNotifications}
                  onClose={() => handleMenuClose("notifications")}
            >
                  <Typography className={classes.text} variant="h5" gutterBottom>
                        Notifications
                        </Typography>
                  <Paper square className={classes.paper}>
                        <List className={classes.list}>
                              {messages.map(({ id, primary, secondary, person, url }) => (
                                    <Fragment key={id}>
                                          {id === 1 && <ListSubheader className={classes.subheader}>Today</ListSubheader>}
                                          {id === 3 && <ListSubheader className={classes.subheader}>Yesterday</ListSubheader>}
                                          <ListItem button onClick={() => goToPage(url)}>
                                                <ListItemAvatar>
                                                      <Avatar alt="Profile Picture" src={person} />
                                                </ListItemAvatar>
                                                <ListItemText primary={primary} secondary={secondary} />
                                          </ListItem>
                                    </Fragment>
                              ))}
                        </List>
                  </Paper>
            </Menu>
      );
      let auth = localStorage.getItem('token') != null ? true : false;


      return (
            <div className={classes.grow}>
                  <AppBar position="static" >
                        <Toolbar style={{height:"50px"}}>
                              <IconButton
                                    edge="start"
                                    className={classes.menuButton}
                                    color="inherit"
                                    aria-label="open drawer"
                              >
                                    <MenuIcon />
                              </IconButton>
                              <Typography className={classes.title} variant="h6" noWrap>
                                    IPCA Manhoso
                              </Typography>
                              <div className={classes.grow} />
                              {auth ? (
                                    <div className={classes.sectionDesktop}>
                                          <IconButton
                                                aria-label="show 17 new notifications"
                                                color="inherit"
                                                aria-controls={menuProfileId}
                                                aria-haspopup="true"
                                                onClick={handleNotificationsMenuOpen}>
                                                <Badge badgeContent={messages.length} color="secondary">
                                                      <NotificationsIcon />
                                                </Badge>
                                          </IconButton>
                                          <IconButton
                                                edge="end"
                                                aria-label="account of current user"
                                                aria-controls={menuNotificationId}
                                                aria-haspopup="true"
                                                onClick={handleProfileMenuOpen}
                                                color="inherit"
                                          >
                                                <AccountCircle />
                                          </IconButton>
                                    </div>
                              ) :
                                    (
                                          <div className={classes.sectionDesktop}>
                                                <Button variant="outlined" href="/register" className={classes.button}>
                                                      Registar
                                                </Button>
                                                <Button variant="outlined" href="/login" className={classes.button}>
                                                      Iniciar Sessão
                                                </Button>
                                          </div>
                                    )}
                        </div>
                        </Toolbar>
                  </AppBar>
                  { renderMenu}
                  { renderNotifications}
            </div >
      )
}

export default NavBar;
