import api from '../services/api';

export const Login = async (user) => {
      const response = await api.post('/auth/login', user);
      return response;
}