import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { fade } from "@material-ui/core/styles/colorManipulator";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import Section from "./Section";
import {yellow} from "@material-ui/core/colors";

// Defining custom button colors using withStyles.
// You can replace yellow with any Material UI color.
const styles = theme => ({
  /**
   * Color styles for text (aka flat) buttons
   * NOTE: these styles are also applied to the outlined varaint.
   * @see https://github.com/mui-org/material-ui/blob/8995f085904eb55bcb5861fb6d8a32fbd38d72eb/packages/material-ui/src/Button/Button.js#L50-L59
   */
  textyellow: {
    color: yellow[800],
    "&:hover": {
      backgroundColor: fade(yellow[100], theme.palette.action.hoverOpacity),
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }
  },

  /**
   * Color styles for outlined buttons.
   * Note: These styles are combined with the text button styles (.textyellow)
   * @see https://github.com/mui-org/material-ui/blob/8995f085904eb55bcb5861fb6d8a32fbd38d72eb/packages/material-ui/src/Button/Button.js#L84-L92
   */
  outlinedyellow: {
    border: `1px solid ${fade(yellow[500], 0.5)}`,
    "&:hover": {
      border: `1px solid ${yellow[500]}`
    },
    // Disabled styles for outlined button...
    // NOTE: You need to pass `classes={{disabled: classes.diabled}}` to
    // the Button component for these styles to work. You also need have
    // a .disabled class in your style rules.
    "&$disabled": {
      border: `1px solid ${theme.palette.action.disabled}`
    }
  },

  /**
   * Color styles for contained (aka raised) buttons
   * @see https://github.com/mui-org/material-ui/blob/8995f085904eb55bcb5861fb6d8a32fbd38d72eb/packages/material-ui/src/Button/Button.js#L131-L141
   */
  containedyellow: {
    color: theme.palette.getContrastText(yellow[500]),
    backgroundColor: yellow[500],
    "&:hover": {
      backgroundColor: yellow[700],
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        backgroundColor: yellow[500]
      }
    }
  },

  // This is required for the '&$disabled' selector to work
  disabled: {}
});

function TextButtons(props) {
  const { classes } = props;
  return [
    <Section key="custom-buttons">
      <Button className={classes.textyellow}>yellow Text</Button>
      <Button
        variant="outlined"
        className={classNames(classes.textyellow, classes.outlinedyellow)}
        classes={{ disabled: classes.disabled }}
      >
        yellow Outlined
      </Button>
      <Button variant="contained" className={classes.containedyellow}>
        yellow Contained
      </Button>
    </Section>,

    <Section key="primary-buttons">
      {["text", "outlined", "contained"].map(variant => (
        <Button color="primary" variant={variant}>
          Primary {variant}
        </Button>
      ))}
    </Section>
  ];
}

TextButtons.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TextButtons);