import Routes from './routes';
import { makeStyles } from '@material-ui/core/styles';
import Footer from './components/Footer';
import NavBar from './components/Navbar';
import {colors} from './utils/colors';


const useStyles = makeStyles((theme) => ({
  footer: {
    position:"fixed",
    bottom: 0,
    width: "100%",
    backgroundColor: colors.default,
    color: colors.white,
    textAlign:"center",
    minHeight:"40px",
  },
  main:{
    marginTop:"50px !important",
    minHeight:"auto",
  }
}));


function App() {
  const classes = useStyles();
  return (
    <>
      <NavBar />
      <div className={classes.main}>
        <Routes />
      </div>
      <div className={classes.footer}>
        <Footer />
      </div>
    </>
  );
}

export default App;
