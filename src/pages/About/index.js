import React from 'react'
import NavBar from '../../components/Navbar'
import Footer from '../../components/Footer'

const About = () => {
      return (
            <>
                  <NavBar />
                  <h1>About</h1>
                  <Footer />
            </>
      )
}

export default About
