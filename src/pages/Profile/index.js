import React, {useState, useEffect} from 'react'
import api from '../../services/api'
import {
  Container,
  Grid,
  Button,
  Avatar,
  TextField,
  IconButton
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import {colors} from '../../utils/colors'
import Message from '../../components/Message';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  paper:{
    textAlign:"center",
  },
  container: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
    marginTop: "2%"
  },
  avatar: {
    margin: "auto",
    height: theme.spacing(20),
    width: theme.spacing(20),
    backgroundColor: colors.default,
    border: "5px solid " + colors.default
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    alignItems: "center",
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  save:{
      margin: theme.spacing(3, 0, 2),
      backgroundColor: colors.default,
      color: colors.white,
      fontWeight: "bold",
      fontSize: "16px",
      "&:hover":{
        backgroundColor: colors.defaultHover
      }
    }
}));



const Profile = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [messageIco, setMessageIco] = useState("success");

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const [errorFirstName, setErrorFirstName] = useState({error:false});
  const [errorLastName, setErrorLastName] = useState({error:false});
  const [errorEmail, setErrorEmail] = useState({error:false});

  async function applyChanges(e) {
        e.preventDefault();
        let check = true;
        if(firstName.length <= 1) {setErrorFirstName({error:true}); check = false;}
        else {setErrorFirstName({error:false});}
        if(lastName.length <= 1) {setErrorLastName({error:true});check = false;}
        else {setErrorLastName({error:false});}
        if(email.length <= 3) {setErrorEmail({error:true});check = false;}
        else {setErrorEmail({error:false});}

        if(!check){
          setMessageIco("error");
          setMessage("Dados invalidos ou incompletos.");
          setOpen(true);
          setTimeout(() => {
            setOpen(false)
          }, 3000)
          return;
        }


        const user = {
          firstName: firstName,
          lastName: lastName,
          email: email
        }
        await api.put('user', user, {
                headers: {
                  'token': localStorage.getItem("token")
                }
              }).then((response) => {
                const status = response.status;
              if (status === 200) {
                    console.log(response.data);
                    setMessage("Dados alterados com sucesso.");
                    setMessageIco("success");
                    setOpen(true);
                setTimeout(() => {
                  setOpen(false)
                }, 3000)
                    // window.location.href = "/";
              }
              else if(status === 401)
                    localStorage.removeItem("token");
                    localStorage.removeItem("expire");
                    console.log(response);
                    // window.location.href ="/";
        })
              .catch(() => {
                    alert("Algo correu mal");
              })

  }

  

  useEffect(() => {
        api.get('user',{
                headers: {
                  'token': localStorage.getItem("token")
                }
              }).then((response) => {
                const status = response.status;
              if (status === 200) {
                    setFirstName(response.data.firstName);
                    setLastName(response.data.lastName);
                    setEmail(response.data.email);
                    console.log(response.data);
              }
              else if(status === 401){
                    localStorage.removeItem("token");
                    localStorage.removeItem("expire");
                    console.log(response);
                    window.location.href ="/";
              }
        })
          .catch((e) => {
            switch (e.response.status) {
              case 401:
                localStorage.removeItem("token");
                localStorage.removeItem("expire");
                alert("A sessão terminou.\n Inicie nova sessão.")
                window.location.href = "/login"
                break;
              default:
                alert("Algo correu mal")
                break;
            }
            window.Location.href = "/"
              })
  },[]);

  const token = localStorage.getItem('token');
  if (token == null) {
        console.log(token);
        debugger;
        window.location.href = "/";
        return <></>;
  }
  
  return (
    <>
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <label htmlFor="contained-button-file">
          <IconButton style={{marginBottom: "50px"}}>
              <Avatar component="button" className={classes.avatar} src="./images/profile_01.jpeg">
              teasad
            </Avatar>
          </IconButton>
        </label>
        
        <form className={classes.form} noValidate onSubmit={applyChanges}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Primeiro Nome"
                onChange={(e) => setFirstName(e.target.value)}
                autoFocus
                value={firstName}
                {...errorFirstName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Ultimo Nome"
                name="lastName"
                autoComplete="lname"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
                {...errorLastName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Correio Eletrônico"
                name="email"
                autoComplete="email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                {...errorEmail}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            className={classes.save}>
            Salvar
          </Button>
        </form>
      </div>
    </Container>
      <Message state={{ open: open, close: setOpen }} message={message} messageIco={messageIco} />
    </>

  );
}

export default Profile
