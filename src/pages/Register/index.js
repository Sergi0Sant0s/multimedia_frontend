import React, { useState } from 'react'
import api from '../../services/api'
import {
  Container,
  Grid,
  Link,
  Button,
  Avatar,
  TextField,
  Typography,
  FormControl,
  InputLabel,
  Select
} from '@material-ui/core'
import {
  LockOutlined as LockOutlinedIcon
} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles';
import {colors} from '../../utils/colors'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    backgroundColor: colors.default,
    marginBottom: "20px",
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: colors.default,
    fontWeight: "bold",
    fontSize: "16px",
    "&:hover":{
      backgroundColor: colors.defaultHover
    }
  },
  login:{
    color: colors.black
  },
  formControl:{
    width: "100%",
    marginTop: "15px"
  }
}));


const Register = () => {
  const classes = useStyles();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [type, setType] = useState("aluno");

  const [errorFirstName, setErrorFirstName] = useState({error:false});
  const [errorLastName, setErrorLastName] = useState({error:false});
  const [errorEmail, setErrorEmail] = useState({error:false});
  const [errorUsername, setErrorUsername] = useState({error:false});
  const [errorPassword, setErrorPassword] = useState({error:false});

  async function register(e) {
        e.preventDefault();
        let check = true;
        if(firstName.length <= 1) {setErrorFirstName({error:true}); check = false;}
        else {setErrorFirstName({error:false});}
        if(lastName.length <= 1) {setErrorLastName({error:true});check = false;}
        else {setErrorLastName({error:false});}
        if(email.length <= 3) {setErrorEmail({error:true});check = false;}
        else {setErrorEmail({error:false});}
        if(username.length <= 3) {setErrorUsername({error:true});check = false;}
        else {setErrorUsername({error:false});}
        if(password.length <= 4) {setErrorPassword({error:true});check = false;}
        else {setErrorPassword({error:false});}
        if(!check)
          return;

        const user = {
          firstName,
          lastName,
          email,
          username,
          password,
          type
        }
        
        await api.post('auth/register', user ).then((response) => {
              if (response.status === 201) {
                    console.log(response.data);
                    window.location.href = "/login";
              }
              else if(response.status === 422)
                    alert("Tem de preencher todos os dados");
        })
              .catch((e) => {
                    alert("Algo correu mal");
              })

  }
  if (localStorage.getItem('token') != null) {
        window.location.href = "/";
        return <></>;
  }



  
  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Junta-te a nós. Regista-te!
        </Typography>
        <form className={classes.form} noValidate onSubmit={register}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Primeiro Nome"
                onChange={(e) => setFirstName(e.target.value)}
                autoFocus
                {...errorFirstName}

              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Ultimo Nome"
                name="lastName"
                autoComplete="lname"
                onChange={(e) => setLastName(e.target.value)}
                {...errorLastName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Correio Eletrônico"
                name="email"
                autoComplete="email"
                onChange={(e) => setEmail(e.target.value)}
                {...errorEmail}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                onChange={(e) => setUsername(e.target.value)}
                {...errorUsername}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
                {...errorPassword}
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-age-native-simple">Tipo de utilizador</InputLabel>
              <Select
                native
                value={type}
                onChange={(e) => {setType(e.target.value)}}
                className={classes.select}
              >
                <option value="aluno">Aluno</option>
                <option value="professor">Professor</option>
              </Select>
            </FormControl>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Registar
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" className={classes.login}>
                Ja tem uma conta? <Link href="/login" className={classes.login}>Iniciar Sessão</Link>
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}

export default Register
