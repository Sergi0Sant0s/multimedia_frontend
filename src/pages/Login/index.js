import React, { useState } from 'react'
import api from '../../services/api'
import {
      Avatar,
      Button,
      FormControlLabel,
      TextField,
      Checkbox,
      Typography,
      makeStyles,
      Container
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {colors} from '../../utils/colors'


const useStyles = makeStyles((theme) => ({
      paper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
      },
      avatar: {
            marginBottom: theme.spacing(3),
            padding: theme.spacing(5),
            backgroundColor: colors.default,
      },
      form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
      },
      submit: {
            margin: theme.spacing(3, 0, 2),
            backgroundColor: colors.default,
            "&:hover":{
                  backgroundColor: colors.defaultHover,
            }

      },
}));


const Login = () => {
      const classes = useStyles();

      const [username, setUsername] = useState("");
      const [password, setPassword] = useState("");

      const [errorUsername, setErrorUsername] = useState({error:false});
      const [errorPassword, setErrorPassword] = useState({error:false});


      async function login(e) {
            e.preventDefault();
            let check = true;
            if(username.length < 3) {setErrorUsername({error:true});check = false;}
            else {setErrorUsername({error:false});}
            if(password.length < 3) {setErrorPassword({error:true});check = false;}
            else {setErrorPassword({error:false});}
            if(!check)
                  return;
            await api.post('auth/login', { username, password }).then((response) => {
                  const status = response.status;
                  if (status === 200) {
                        localStorage.setItem("expire", response.data.exp);
                        localStorage.setItem("token", response.data.token);
                        window.location.href = "/";
                  }
            })
            .catch((e) => {
                  switch (e.response.status) {
                        case 400:
                              alert("Username ou password invalidos.")
                              break;
                        default:
                              alert("Algo correu mal")
                              break;
                  }
            })

      }
      if (localStorage.getItem('token') != null) {
            window.location.href = "/";
            return <></>;
      }



      return (
            <Container component="main" maxWidth="xs">
                  <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                              <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                              Iniciar Sessão
        </Typography>
                        <form className={classes.form} noValidate onSubmit={login}>
                              <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="username"
                                    label="Username"
                                    name="username"
                                    onChange={(e) => setUsername(e.target.value)}
                                    autoFocus
                                    {...errorUsername}
                              />
                              <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    onChange={(e) => setPassword(e.target.value)}
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    {...errorPassword}
                              />
                              <FormControlLabel
                                    control={<Checkbox value="remember" color="default"/>}
                                    label="Remember me"
                              />
                              <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                              >
                                    Iniciar Sessão
          </Button>
                        </form>
                  </div>
            </Container>
      );
}

export default Login
