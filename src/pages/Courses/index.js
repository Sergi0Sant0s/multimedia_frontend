import React, {useState, useEffect} from 'react'
import api from '../../services/api'
import {
  DataGrid
} from '@material-ui/data-grid'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  ThemeProvider,
  Typography,
  TextField
} from '@material-ui/core'
import { green, red, yellow } from '@material-ui/core/colors';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { fade } from "@material-ui/core/styles/colorManipulator";
import Message from '../../components/Message';


const useStyles = makeStyles((theme) => ({
  buttons:{
    margin:"5px"
  },
  buttonEdit: {
    color: yellow[900],
    margin: "5px",
    "&:hover": {
      backgroundColor: fade(yellow[900], theme.palette.action.hoverOpacity),
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }
    },
  mainButtons:{
    float:"right",
    marginTop:"10px"
  }
}));

const theme = createMuiTheme({
  palette: {
    add: green,
    edit: yellow,
    delete: red,
  },
});

const Courses = () => {
  const classes = useStyles();
  const [openMessage, setOpenMessage] = useState(false);
  const [message, setMessage] = useState("");
  const [messageIco, setMessageIco] = useState("success");
 const [rows, setRows] = useState([]);
 const [open, setOpen] = React.useState(false);
 const [course, setEditCourse] = React.useState({});
 const [confirmButton, setConfirmButton] = React.useState("Novo Curso");
 
const columns = [
  { field: 'id', headerName: 'ID', width: "30vh",  },
  { field: 'name', headerName: 'Nome do Curso', width: "60vh" }
];


useEffect(() => {
        api.get('course',{
                headers: {
                  'token': localStorage.getItem("token")
                }
              }).then((response) => {
                const status = response.status;
              if (status === 200) {

                    setRows(response.data.map((course) => {
                      return {id:course._id,name:course.name}
                    }));
                    console.log(response.data);
              }
        })
              .catch((e) => {
                if (e.response.status !== undefined) {
                  console.log(e.response.data);
                  switch (e.response.status) {
                        case 401:
                      localStorage.removeItem("token");
                      localStorage.removeItem("expire");
                      alert("A sessão terminou.\n Inicie nova sessão.")
                      window.location.href = "/login"
                      break;
                        default:
                      alert("Algo correu mal")
                      break;
                  }
                  window.Location.href = "/"
                }
                else {
                  alert("Algo correu mal")
                  window.Location.href = "/"

                }
              })
  },[]);

  

  const handleClickOpen = (type) => {
    switch (type) {
      case "novo":
        setConfirmButton("Novo Curso");
        setEditCourse({name:""})
        setOpen(true);
        break;
      case "edit":
        setConfirmButton("Salvar");
        if(course.name === undefined || course.name === "")
          alert("Tem de selecionar um curso.");
        else
        setOpen(true);
        break;
      case "delete":
        if(course.name === undefined || course.name === "")
          alert("Tem de selecionar um curso.");
        else
          deleteCourse();
        break;
      default:
        break;
    }
  };

  const newCourse = (e) =>{
    e.preventDefault();
    if(confirmButton === "Novo Curso"){
              api.post('course',course,{headers: {'token': localStorage.getItem("token")}}).then((response) => {
              console.log(response.status);
              
              const status = response.status;
              if (status === 201) {
                    console.log(response.data);
                    setRows([...rows,{
                      id : response.data._id,
                      name : response.data.name
                    }]);
                setMessage("Curso " + course.name + " criado com sucesso.");
                setMessageIco("success");
                setOpenMessage(true);
                setTimeout(() => {
                  setOpenMessage(false)
                }, 3000)
                handleClose();
                setEditCourse("");
              }
              })
              .catch((e) => {
                    console.log(e);
                    switch (e.response.status) {
                        case 401:
                              localStorage.removeItem("token");
                              localStorage.removeItem("expire");
                              alert("A sessão terminou.\n Inicie nova sessão.")
                              window.location.href="/login"
                              break;
                        case 409:
                        setMessage("O curso ja existe.");
                        setMessageIco("error");
                        setOpenMessage(true);
                        setTimeout(() => {
                          setOpenMessage(false)
                        }, 3000)
                        handleClose();
                              break;
                        default:
                        setMessage("Algo correu mal.");
                        setMessageIco("error");
                        setOpenMessage(true);
                        setTimeout(() => {
                          setOpenMessage(false)
                        }, 3000)
                        handleClose();
                              break;
                  }
                  window.Location.href="/"
              })
            }else{
              console.log(course);
              api.put("course/"+course.id,{name:course.name},{headers: {'token': localStorage.getItem("token")}}).then((response) => {
              
              const status = response.status;
              if (status === 200) {
                    const aux = rows.map((item) => {
                      return item.id === course.id ?
                         course : item;
                    })
                    setRows(aux);
                setMessage("Curso " + course.name + " atualizado com sucesso.");
                setMessageIco("success");
                setOpenMessage(true);
                setTimeout(() => {
                  setOpenMessage(false)
                }, 3000)
                    handleClose();
                    console.log(response.data);
              }
              })
              .catch((e) => {
                    switch (e.response.status) {
                        case 401:
                              localStorage.removeItem("token");
                              localStorage.removeItem("expire");
                              alert("A sessão terminou.\n Inicie nova sessão.")
                              window.location.href="/login"
                              break;
                        default:
                        setMessage("Algo correu mal.");
                        setMessageIco("error");
                        setOpenMessage(true);
                        setTimeout(() => {
                          setOpenMessage(false)
                        }, 3000)
                        handleClose();
                              break;
                  }
                  window.Location.href="/"
              })
            }
  }

  const handleClose = () => {
    setOpen(false);
  };

  const deleteCourse = () => {
    if(window.confirm("Pretende eliminar o curso " + course.name + "?")){
      api.delete("course/"+course.id,{headers: {'token': localStorage.getItem("token")}}).then((response) => {
              
              const status = response.status;
              if (status === 200) {
                    let aux = [];
                    rows.forEach((item) => {
                      if(item.id !== course.id) {
                        aux.push(item);
                      }
                    })
                    setRows(aux);
                    setEditCourse({})
                setMessage("Curso eliminado com sucesso.");
                setMessageIco("success");
                setOpenMessage(true);
                setTimeout(() => {
                  setOpenMessage(false)
                }, 3000)
              }
              })
              .catch((e) => {
                    switch (e.response.status) {
                        case 401:
                              localStorage.removeItem("token");
                              localStorage.removeItem("expire");
                              alert("A sessão terminou.\n Inicie nova sessão.")
                              window.location.href="/login"
                              break;
                        default:
                              alert("Algo correu mal")
                              break;
                  }
                  window.Location.href="/"
              })
    }
  }

  return (
    <>
    <div style={{ height: "430px", width: '50%', margin:"auto" }}>
      <Typography variant="h3" component="h2" style={{marginBottom:"20px"}}>
          Cursos
        </Typography>

        <DataGrid rows={rows} columns={columns} pageSize={6} checkboxSelection={false} onRowSelected={(newSelection) => {
                                      setEditCourse(newSelection.data);
        }}
          border={{ border: "1px solid black !important" }} />

      <div className={classes.mainButtons}>
        <ThemeProvider theme={theme}>
          <Button variant="outlined" color="primary" onClick={() => handleClickOpen("novo")} className={classes.buttons}>
            Novo Curso
          </Button>
          <Button variant="outlined" color="edit" onClick={() => handleClickOpen("edit")} className={classes.buttonEdit}>
            Editar Curso
          </Button>
          <Button variant="outlined" color="secondary" onClick={() => handleClickOpen("delete")} className={classes.buttons}>
            Eliminar Curso
          </Button>
        </ThemeProvider>
      </div>
    </div>
      <Message state={{ open: openMessage, close: setOpenMessage }} message={message} messageIco={messageIco} />
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
            {(confirmButton === "Novo Curso") ?
              ("Novo Curso")
              :
              ("Editar Curso")
            }
          </DialogTitle>
        <DialogContent style={{margin:"20px"}}>
         <form className={classes.form} /* noValidate onSubmit={login} */>
                              <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="course"
                                    label="Curso"
                                    name="course"
                                    onChange={(e) => {
                                      course.id ? 
                                      setEditCourse({id:course.id,name: e.target.value}) :
                                      setEditCourse({name: e.target.value})
                                    }}
                                    autoFocus
                                    value={course.name}
                              />
                        </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button color="primary" onClick={newCourse}>
            {confirmButton}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    </>
  )
}

export default Courses
