import React, { useState, useEffect } from 'react'
import api from '../../services/api'
import {
  DataGrid
} from '@material-ui/data-grid'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  ThemeProvider,
  Typography,
  TextField,
  Grid,
  FormControl,
  InputLabel,
  Select
} from '@material-ui/core'
import { green, red, yellow } from '@material-ui/core/colors';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { fade } from "@material-ui/core/styles/colorManipulator";
import Message from '../../components/Message';


const useStyles = makeStyles((theme) => ({
  buttons: {
    margin: "5px"
  },
  buttonEdit: {
    color: yellow[900],
    margin: "5px",
    "&:hover": {
      backgroundColor: fade(yellow[900], theme.palette.action.hoverOpacity),
      "@media (hover: none)": {
        backgroundColor: "transparent"
      }
    }
  },
  formControl: {
    width: "100%"
  },
  mainButtons: {
    float: "right",
    marginTop: "10px"
  }
}));

const theme = createMuiTheme({
  palette: {
    add: green,
    edit: yellow,
    delete: red,
  },
});

const Subjects = () => {
  const classes = useStyles();
  const [openMessage, setOpenMessage] = useState(false);
  const [message, setMessage] = useState("");
  const [messageIco, setMessageIco] = useState("success");

  const [courses, setCourses] = useState([]);
  const [rows, setRows] = useState([]);
  const [open, setOpen] = useState(false);
  const [subject, setEditSubject] = useState({});
  const [confirmButton, setConfirmButton] = useState("Nova Disciplina");

  const columns = [
    { field: 'id', headerName: 'ID', width: "30vh", },
    { field: 'name', headerName: 'Disciplina', width: "30vh" },
    { field: 'courseName', headerName: 'Curso', width: "30vh" }
  ];


useEffect(() => {
  api.get('course', {
    headers: {
      'token': localStorage.getItem("token")
    }
  }).then((response) => {
    const status = response.status;
    if (status === 200) {
      const aux = response.data.map((item) => {
        return {
          id: item._id,
          name: item.name
        }
      });
      setCourses(aux);
    }
  })
    .catch((e) => {

      switch (e.response.status) {
        case 401:
          localStorage.removeItem("token");
          localStorage.removeItem("expire");
          alert("A sessão terminou.\n Inicie nova sessão.")
          window.location.href = "/login"
          break;
        default:
          alert("Algo correu mal")
          break;
      }
      window.Location.href = "/"
    })
  api.get('subject', {
    headers: {
      'token': localStorage.getItem("token")
    }
  }).then((response) => {
    const status = response.status;
    if (status === 200) {
      try {
        setRows(response.data);
        console.log(rows);
      } catch (e) {
        console.log(e);
        alert("Algo correu mal");
      }
    }
  })
    .catch((e) => {
      console.log(e);
      if (e.response.status !== undefined)
        switch (e.response.status) {
          case 401:
            localStorage.removeItem("token");
            localStorage.removeItem("expire");
            alert("A sessão terminou.\n Inicie nova sessão.")
            window.location.href = "/login"
            break;
          default:
            alert("Algo correu mal")
            break;
        }
      else
        alert("Algo correu mal");

      window.Location.href = "/"
    })
}, []);



  const handleClickOpen = (type) => {
    switch (type) {
      case "novo":
        setConfirmButton("Nova Disciplina");
        setEditSubject({ name: "", courseId: courses[0].id })
        setOpen(true);
        break;
      case "edit":
        setConfirmButton("Salvar");
        setOpen(true);
        break;
      case "delete":
        if (subject.name === undefined || subject.name === "")
          alert("Tem de selecionar um disciplina.");
        else
          deleteCourse();
        break;
      default:
        break;
    }
  };

  const newSubject = (e) => {
    e.preventDefault();
    console.log(confirmButton)
    if (confirmButton === "Nova Disciplina") {
      console.log(subject)
      api.post('subject', subject, { headers: { 'token': localStorage.getItem("token") } }).then((response) => {
        console.log("teste");
        console.log(response.status);

        const status = response.status;
        if (status === 201) {
          console.log(response.data);
          setRows([...rows, response.data]);
          console.log(rows);
          handleClose();
          setEditSubject({});
          setMessage("Disciplina " + subject.name + " criada com sucesso.");
          setMessageIco("success");
          setOpenMessage(true);
          setTimeout(() => {
            setOpenMessage(false)
          }, 3000)
        }
      })
        .catch((e) => {
          if (e.response.status !== undefined) {
            console.log(e);
            switch (e.response.status) {
              case 401:
                localStorage.removeItem("token");
                localStorage.removeItem("expire");
                alert("A sessão terminou.\n Inicie nova sessão.")
                window.location.href = "/login"
                break;
              case 409:
                setMessage("Disciplina ja existe.");
                setMessageIco("error");
                setOpenMessage(true);
                setTimeout(() => {
                  setOpenMessage(false)
                }, 3000)
                break;
              default:
                alert("Algo correu mal")
                break;
            }
            window.Location.href = "/"
          }
          else {
            alert("Algo correu mal")
            window.Location.href = "/"

          }
        })
    } else if (confirmButton === "Salvar") {
      console.log(subject);
      api.put("subject/" + subject.id, { name: subject.name, courseId: subject.courseId }, { headers: { 'token': localStorage.getItem("token") } }).then((response) => {
        if (response.status === 200) {
          let aux = [];
          rows.forEach((item) => {
            item.id === subject.id ?
              aux.push(subject) : aux.push(item);
          })
          setOpen(false);
          setRows(aux);
          //
          setMessage("Disciplina " + subject.name + " atualizada com sucesso.");
          setMessageIco("success");
          setOpenMessage(true);
          setTimeout(() => {
            setOpenMessage(false)
          }, 3000)
        }
      })
        .catch((e) => {
          if (e.response.status !== undefined) {
            console.log(e.response.data);
            switch (e.response.status) {
              case 401:
                localStorage.removeItem("token");
                localStorage.removeItem("expire");
                alert("A sessão terminou.\n Inicie nova sessão.")
                window.location.href = "/login"
                break;
              default:
                alert("Algo correu mal")
                break;
            }
            window.Location.href = "/"
          }
          else {
            alert("Algo correu mal")
            window.Location.href = "/"

          }
        })
    }
  }

  const handleClose = () => {
    setOpen(false);
  };

  const deleteCourse = () => {
    if (window.confirm("Pretende eliminar a disciplina " + subject.name + "?")) {
      api.delete("subject/" + subject.id, { headers: { 'token': localStorage.getItem("token") } }).then((response) => {

        const status = response.status;
        if (status === 200) {
          let aux = [];
          rows.forEach((item) => {
            if (item.id !== subject.id) {
              aux.push(item);
            }
          })
          setMessage("Disciplina " + subject.name + " eliminada com sucesso.");
          setMessageIco("success");
          setOpenMessage(true);
          setRows(aux);
          setEditSubject({})
        }
      })
        .catch((e) => {
          switch (e.response.status) {
            case 401:
              localStorage.removeItem("token");
              localStorage.removeItem("expire");
              alert("A sessão terminou.\n Inicie nova sessão.")
              window.location.href = "/login"
              break;
            default:
              alert("Algo correu mal")
              break;
          }
          window.Location.href = "/"
        })
    }
  }

  return (
    <>
      <div style={{ height: "430px", width: '50%', margin: "auto" }}>
        <Typography variant="h3" component="h2" style={{ marginBottom: "20px" }}>
          Disciplinas
        </Typography>
        <DataGrid rows={rows} columns={columns} pageSize={6} checkboxSelection={false} onRowSelected={(newSelection) => {
          console.log(newSelection.data)
          setEditSubject(newSelection.data);
        }} />
        <div className={classes.mainButtons}>
          <ThemeProvider theme={theme}>
            <Button variant="outlined" color="primary" onClick={() => handleClickOpen("novo")} className={classes.buttons}>
              Novo Curso
            </Button>
            <Button variant="outlined" color="edit" onClick={() => handleClickOpen("edit")} className={classes.buttonEdit} disabled={subject.name === undefined ? true : false}>
              Editar Curso
            </Button>
            <Button variant="outlined" color="secondary" onClick={() => handleClickOpen("delete")} className={classes.buttons}>
              Eliminar Curso
            </Button>
          </ThemeProvider>
        </div>
      </div>
      <Message state={{ open: openMessage, close: setOpenMessage }} message={message} messageIco={messageIco} />
      <div>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">{confirmButton === "Nova Disciplina" ? 'Novo' : 'Editar'} Disciplina</DialogTitle>
          <DialogContent style={{ margin: "20px" }}>
            <form className={classes.form} /* noValidate onSubmit={login} */>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="disciplina"
                label="Disciplina"
                name="disciplina"
                onChange={(e) => {
                  subject.id ?
                    setEditSubject({ id: subject.id, name: e.target.value, courseId: subject.courseId, courseName: subject.courseName }) :
                    setEditSubject({ name: e.target.value, courseId: subject.courseId, courseName: subject.courseName })
                }}
                autoFocus
                value={subject.name}
              />
              <Grid item xs={12}>
                <FormControl variant="filled" className={classes.formControl}>
                  <InputLabel htmlFor="filled-age-native-simple">Curso</InputLabel>
                  <Select
                    native
                    value={subject.courseId}
                    onChange={(e) => {
                      setEditSubject(subject.id === undefined ?
                        {
                          name: subject.name,
                          courseId: e.target.value,
                          courseName: e.target[e.target.selectedIndex].text
                        } :
                        {
                          id: subject.id,
                          name: subject.name,
                          courseId: e.target.value,
                          courseName: e.target[e.target.selectedIndex].text,
                        })
                    }}
                    className={classes.select}
                  >
                    {courses.map((item) => {
                      return <option value={item.id}>{item.name}</option>

                    })}
                  </Select>
                </FormControl>
              </Grid>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button color="primary" onClick={newSubject}>
              {confirmButton}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  )
}

export default Subjects
